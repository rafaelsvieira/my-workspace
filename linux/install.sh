#!/bin/bash
set -xe

BASEDIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
CMD_INSTALL=""
BASH_FILE=""

install_tig() {
  git clone https://github.com/jonas/tig.git
  cd tig
  make prefix=/usr/local
  make install prefix=/usr/local
  cd -
  rm -rf tig
}

install_vundle() {
  git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim
  #vim +PlugInstall +qall
}

install_tmux_ressurect() {
  git clone https://github.com/tmux-plugins/tmux-resurrect $HOME/projects/.tmux-resurrect
}

install_dependencies() {
  check_vendor
  eval $CMD_INSTALL $(cat common_deps.txt)
}

configure_workspace() {
  rsync .bashrc $HOME/$BASH_FILE
  rsync .tmux.conf $HOME
  rsync .vimrc $HOME

}

check_vendor() {
  set +e
  YUM_CMD=$(find /usr/ -name "yum" | grep yum)
  APT_CMD=$(find /usr/ -name "apt-get" | grep "apt-get")

  if [[ ! -z $YUM_CMD ]]; then
    CMD_INSTALL="yum install -y"
    BASH_FILE=".bash_profile"
    yum update -y
    eval $CMD_INSTALL $(cat centos_deps.txt)
  elif [[ ! -z $APT_CMD ]]; then
    CMD_INSTALL="apt-get install -y"
    BASH_FILE=".bashrc"
    apt-get update
    eval $CMD_INSTALL $(cat ubuntu_deps.txt)
  else
    echo "NOT FOUND VENDOR TO INSTALL DEPENDENCIES! Only support to yum and apt-get"
    exit 1
  fi
  set -e
}

main() {
  cd $BASEDIR
  install_dependencies
  configure_workspace
  mkdir -p $HOME/projects
  install_tig
  install_vundle
  install_tmux_ressurect
}

main
