set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'            " Show modes editions into vim
Plugin 'vim-airline/vim-airline-themes'     " Themes

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Put your non-Plugin stuff after this line

" General
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
set nocompatible
" use indentation of previous line
" set autoindent
" use intelligent indentation for C
" set smartindent
" configure tabwidth and insert spaces instead of tabs
set tabstop=2        " tab width is 4 spaces
set shiftwidth=2     " indent also with 4 spaces
set expandtab        " expand tabs to spaces
set textwidth=0
set wrapmargin=0
set wrap
set linebreak
set nolist  " list disables linebreak
set t_Co=256
set number

highlight ExtraWhitespace ctermbg=red
match ExtraWhitespace /\s\+$/

" Open new file and not close NERDTree
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

syntax on

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
" let g:ycm_collect_identifiers_from_tags_files = 1

" Airline
let g:airline_powerline_fonts=1
let g:airline_theme='badwolf'

" NERDTRee
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | vertical resize +20 | endif

set ttymouse=xterm2
set mouse=a
